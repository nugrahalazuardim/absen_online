<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('welcome');
});

Route::get('/index', function() {
    return view('layout.page.index');
});

Route::get('/admin', function(){
    return view('layout.admin');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::get('student/home', 'HomeController@studentHome')->name('student.home')->middleware('is_student');

Route::get('/student', 'studentController@index');
Route::get('/student/create', 'studentController@create');
Route::post('/student', 'studentController@store');
Route::get('/student/{student_id}', 'studentController@show');
Route::get('/student/{student_id}/edit', 'studentController@edit');
Route::put('student/{student_id}', 'studentController@update');
Route::delete('student/{student_id}', 'studentController@destroy');

Route::resource('/learn', 'LearnController');