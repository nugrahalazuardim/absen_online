<H1>APLIKASI ABSENSI PRESENSI ONLINE</H1>
<H2>Last Project Sanbercode Laravel Batch-25</H2>

<H3>Team : Adi Priyanto (ADI), Lazuardi Nugraha (LAZUARDI), Rachmatulloh Bryan AS (BRYAN)</H3>

<H3>Komponen Aplikasi & Penugasan</H3>
<OL type="A">
  <li>GIT Inisial Project Laravel 6 (LAZUARDI)</li>
  <li>Template ELEGAN (LAZUARDI)</li>
  <li>Template ADMINLTE3 (BRYAN)</li>
  <li>Multi Auth Middleware (ADI)</li>
  <li>Table USERS (ADI)</li>
  <li>Table EVENTS (LAZUARDI)</li>
  <li>Table PRESENTS (ADI)</li>
  <li>Table STUDENTS (BRIAN)</li>
  <li>Table COURSES (LAZUARDI)</li>
  <li>Table LEARNS (BRIAN)</li>
  <li>Lib. EXCEL (BRIAN)</li>
  <li>Lib. PDF (LAZUARDI)</li>
  <li>Lib. SIGN-PAD (ADI)</li>
</OL>

<img src="erd.png" alt="ERD" width="500" height="600">

... keep going.. keep going ...!!!