<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Learn;
use App\Course;

class studentController extends Controller
{
    public function index(){
        $student= DB::table('students')->get();
        return view('students.index', compact('student'));
    }

    public function create(){
        return view('students.create');
    }

    public function store(Request $request){
        $request->validate([
            'type' => 'required|unique:students',
            'name' => 'required',
            'email' => 'required',
            'course' => 'required',
            'whatsapp' => 'required',
            'address' => 'required',
        ]);
        $query = DB::table('students')->insert([
            "type" => $request["type"],
            "name" => $request["name"],
            "email" => $request["email"],
            "course" => $request["course"],
            "whatsapp" => $request["whatsapp"],
            "address" => $request["address"]
        ]);
        return redirect('/student');
    }

    public function show($id){
        $student= DB::table('students')->where('id', $id)->first();
        return view('students.show', compact('student'));
    }

    public function edit($id){
        $student= DB::table('students')->where('id', $id)->first();
        return view('students.edit', compact('student'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'type' => 'required|unique:students',
            'name' => 'required',
            'email' => 'required',
            'course' => 'required',
            'whatsapp' => 'required',
            'address' => 'required',
        ]);
        $query = DB::table('students')
            ->where('id', $id)
            ->update([
            "type" => $request["type"],
            "name" => $request["name"],
            "email" => $request["email"],
            "course" => $request["course"],
            "whatsapp" => $request["whatsapp"],
            "address" => $request["address"]
            ]);
        return redirect('/student');
    }

    public function destroy($id){
        $query = DB::table('students')->where('id', $id)->delete();
        return redirect('/student');
    }
}
