@extends('layout.admin')

@section('tittle')
   Student
@endsection

@section('content')
    
<a href="/student/create" class="btn btn-primary mb-2">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">tipe</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Course</th>
                <th scope="col">Whatsapp</th>
                <th scope="col">Address</th>
                <th scope="col">Detailed</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($student as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->type}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->email}}</td>
                        <td>{{$value->course}}</td>
                        <td>{{$value->whatsapp}}</td>
                        <td>{{$value->address}}</td>
                        <td>
                            <a href="/student/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/student/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/student/{{$value->id}}" method="POST">
                                 @csrf
                                 @method('DELETE')
                                 <input type="submit" class="btn btn-danger my-1" value="Delete">
                             </form>
                         </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection