@extends('layout.admin')

@section('tittle')
    student
@endsection

@section('content')
<div>
    <form action="/student" method="POST">
        @csrf
        <div class="form-group">
            <label>Type</label>
            <input type="text" class="form-control" name="type" placeholder="Masukkan Body">
            @error('type')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" placeholder="Masukkan Body">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" placeholder="Masukkan Body">
            @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Course</label>
            <input type="text" class="form-control" name="course" placeholder="Masukkan Body">
            @error('course')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Whatsapp</label>
            <input type="text" class="form-control" name="whatsapp" placeholder="Masukkan Body">
            @error('whatsapp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" name="address" placeholder="Masukkan Body">
            @error('address')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection