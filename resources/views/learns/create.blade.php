@extends('layout.admin')

@section('tittle')
   
@endsection

@section('content')
<div>
    <form action="/learn" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="form-group">
            <label>Date Start</label>
            <input type="date" class="form-control" name="datestart" placeholder="Masukkan Tanggal Mulai">
            @error('datestart')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Date End</label>
            <input type="date" class="form-control" name="dateend" placeholder="Masukkan Tanggal Berakhir">
            @error('dateend')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Student</label>
           <select class="form-control" name="students_id" id="exampleFormControlSelect1">
               <option value="">--Select Type--</option>
               @foreach ($student as $item)
                   <option value="{{$item->id}}">{{$item->type}}</option>
               @endforeach
           </select>
            @error('students_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection